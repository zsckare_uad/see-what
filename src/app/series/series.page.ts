import { Component, OnInit } from '@angular/core';
import axios from 'axios';
import { Router, NavigationExtras } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-series',
  templateUrl: './series.page.html',
  styleUrls: ['./series.page.scss'],
})
export class SeriesPage implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private router: Router ) { }
  movies = []
  query = "";
  avanzado = false 
  years = []
  genres =  [
      {
        id: 28,
        name: "Acción"
      },
      {
        id: 12,
        name: "Aventura"
      },
      {
        id: 16,
        name: "Animación"
      },
      {
        id: 35,
        name: "Comedia"
      },
      {
        id: 80,
        name: "Crimen"
      },
      {
        id: 99,
        name: "Documental"
      },
      {
        id: 18,
        name: "Drama"
      },
      {
        id: 10751,
        name: "Familia"
      },
      {
        id: 14,
        name: "Fantasía"
      },
      {
        id: 36,
        name: "Historia"
      },
      {
        id: 27,
        name: "Terror"
      },
      {
        id: 10402,
        name: "Música"
      },
      {
        id: 9648,
        name: "Misterio"
      },
      {
        id: 10749,
        name: "Romance"
      },
      {
        id: 878,
        name: "Ciencia ficción"
      },
      {
        id: 10770,
        name: "Película de TV"
      },
      {
        id: 53,
        name: "Suspense"
      },
      {
        id: 10752,
        name: "Bélica"
      },
      {
        id: 37,
        name: "Western"
      }
  ]
  busqueda_avanzada = {
    year: "",
    genre: "",
    director: "",
    duration: ""
  }

  ngOnInit() {
    this.getPopularMovies()
    this.generateYears()
  }
  abrirBusqueda(){
    this.avanzado = !this.avanzado
  }


  buscarPelicula(){
    if(this.query.length > 0){
    axios.get("https://api.themoviedb.org/3/search/tv?api_key=b9b712453a03e546d6e637f0f91a2c93&language=es-MX&query="+this.query+"&page=1" ).then(data=>{
      console.log(data.data.results)
      this.movies = data.data.results
    })}else{
      this.getPopularMovies()
    }
  }

  generateYears(){
   
    let date = new Date()
    let year = date.getFullYear()
    for(let i = year; i >= 1970; i--){
      this.years.push(i)
    }
    
    console.log(this.years)
  }
  buscarPeliculaConfiltros(){
    console.log(this.busqueda_avanzada)
    let url = "https://api.themoviedb.org/3/discover/tv?api_key=b9b712453a03e546d6e637f0f91a2c93&language=es-MX&sort_by=popularity.desc&include_adult=false&include_video=false&page=1"
    if(this.busqueda_avanzada.year != ""){
      url = url + "&year="+this.busqueda_avanzada.year
    }
    if(this.busqueda_avanzada.genre != ""){
      url = url + "&with_genres="+this.busqueda_avanzada.genre
    }
    if(this.busqueda_avanzada.director != ""){
      url = url + "&with_crew="+this.busqueda_avanzada.director
    }
    if(this.busqueda_avanzada.duration != ""){
      url = url + "&with_runtime.lte="+this.busqueda_avanzada.duration
    }
    console.log(url)
    axios.get(url).then(data=>{
      console.log(data.data)
      this.movies = data.data.results
    })
  }
  getPopularMovies(){
    axios.get("https://api.themoviedb.org/3/tv/popular?api_key=b9b712453a03e546d6e637f0f91a2c93&language=es-MX&page=1" ).then(data=>{
      console.log(data.data)
      this.movies = data.data.results
    })
  }
  getPoster(poster){
    return "https://image.tmdb.org/t/p/w500/"+poster
  }
  goToMovie(movie){
    console.log(movie)
    let navigationExtras: NavigationExtras = {
      state: {
        special: JSON.stringify(movie)
      }
    };
    this.router.navigate(['item'], navigationExtras);
  }

}
