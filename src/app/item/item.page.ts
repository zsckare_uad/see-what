import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-item',
  templateUrl: './item.page.html',
  styleUrls: ['./item.page.scss'],
})
export class ItemPage implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router,private navCtrl:NavController) { }
  data: any;
  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.data = JSON.parse(this.router.getCurrentNavigation().extras.state.special);
        console.log(this.data);
      }
    });
  }
  getPoster(){
    return "https://image.tmdb.org/t/p/w500/"+this.data.poster_path;
  }
  getYear(){
    return this.data.release_date.split("-")[0];
  }
}
