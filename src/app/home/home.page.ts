import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { present } from '@ionic/core/dist/types/utils/overlays';
import axios from 'axios';
import { Router, NavigationExtras } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  buscando = true
  query = ""
  results = []
  constructor(private activatedRoute: ActivatedRoute, private router: Router ) { }

  ngOnInit() { 
  }

  busquedaMixta(){
    var url = "https://api.themoviedb.org/3/search/multi?api_key=b9b712453a03e546d6e637f0f91a2c93&language=es-MX&query="+this.query+"&page=1&include_adult=false"

    axios.get(url).then((data)=>{
      console.log(data.data)
      this.buscando = false
this.results = data.data.results
    })

  }
  getPoster(poster){
    return "https://image.tmdb.org/t/p/w500/"+poster
  }

  goToMovie(movie){
    console.log(movie)
    let navigationExtras: NavigationExtras = {
      state: {
        special: JSON.stringify(movie)
      }
    };
    this.router.navigate(['item'], navigationExtras);
  }

  clear(){
    this.query = ""
    this.results = []
    this.buscando = true
  }
   
}
